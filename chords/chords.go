package chords

import (
	"music-theory/constant"
	"music-theory/scales"
)

type Chord struct {
	Scale     scales.Data
	CName     string
	BassNote  int
	ChordName string
}

func New() Chord {
	c := Chord{}
	return c
}

func (c *Chord) Init(root, mode int) {
	c.Scale.Mode = mode
	c.Scale.Init(root)
	c.BassNote = root
}

func (c *Chord) GetChordName(degree int) {

	t := c.Scale.Notes[c.Scale.RootIndex+degree]
	c.ChordName = c.Scale.Chromatics[c.Scale.ChromaticNumber][t%12]
	//fmt.Println("-----", c.ChordName, "-----")
	switch c.Scale.Mode {
	case constant.Ionian:
		switch degree {
		case 1, 2, 5:
			c.ChordName += "m"
		case 6:
			c.ChordName += "dim"
		}
	case constant.Aeolian:
		switch degree {
		case 0, 3, 4:
			c.ChordName += "m"
		case 1:
			c.ChordName += "dim"
		}
	}
}

func (c *Chord) BuildTriad(degree, inversion int) {

}

func (c *Chord) Triad(degree, inversion int) string {
	var t [3]int
	c.CName = ""
	switch inversion {
	case 0:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+2+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
		}
	case 1:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+2+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+7+degree]
		}
	case 2:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+7+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+9+degree]
		}
	}

	c.BassNote = t[0] - 12
	if c.BassNote >= 60 {
		c.BassNote -= 12
	}

	//c.CName += " "
	var tt [3]int
	//var ret string
	//ret = "<"
	tt[0] = t[0]
	tt[1] = t[1]
	tt[2] = t[2]
	if tt[0] > 71 {
		for j := 0; j < 3; j++ {
			tt[j] = tt[j] - 12
		}
	}

	for i := 0; i < 3; i++ {
		c.CName += c.Scale.Chromatics[c.Scale.ChromaticNumber][t[i]%12]
		if i == 2 {
			break
		}
		c.CName += ","
	}
	return c.CName
}

func (c *Chord) MinorTriad(degree, inversion int) string {
	var t [3]int

	switch inversion {
	case 0:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+2+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
			c.BassNote = t[0] - 12
			if c.BassNote >= 60 {
				c.BassNote -= 12
			}
			c.CName = c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			c.CName += "1"
			switch degree {
			case 1, 2, 5:
				c.CName += ":m"
			case 6:
				c.CName += ":dim"
			}
		}
	case 1:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+2+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+7+degree]
			c.BassNote = t[0] - 12
			if c.BassNote >= 60 {
				c.BassNote -= 12
			}
			c.CName = c.Scale.Chromatics[c.Scale.ChromaticNumber][t[2]%12]
			c.CName += "1"
			switch degree {
			case 0:
				c.CName += "/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 1, 2, 5:
				c.CName += ":m/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 3, 4:
				c.CName += ":/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 6:
				c.CName += ":dim/+ c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]"
			}

		}
	case 2:
		{
			t[0] = c.Scale.Notes[c.Scale.RootIndex+4+degree]
			t[1] = c.Scale.Notes[c.Scale.RootIndex+7+degree]
			t[2] = c.Scale.Notes[c.Scale.RootIndex+9+degree]
			c.BassNote = t[0] - 12
			if c.BassNote >= 60 {
				c.BassNote -= 12
			}
			c.CName = c.Scale.Chromatics[c.Scale.ChromaticNumber][t[1]%12]
			c.CName += "1"
			switch degree {
			case 0:
				c.CName += "/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 1, 2, 5:
				c.CName += ":m/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 3, 4:
				c.CName += ":/" + c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]
			case 6:
				c.CName += ":dim/+ c.Scale.Chromatics[c.Scale.ChromaticNumber][t[0]%12]"
			}

		}

	}
	var tt [3]int
	var ret string
	//ret = "<"
	tt[0] = t[0]
	tt[1] = t[1]
	tt[2] = t[2]
	if tt[0] > 71 {
		for j := 0; j < 3; j++ {
			tt[j] = tt[j] - 12
		}
	}

	for i := 0; i < 3; i++ {
		//ret += c.Scale.Chromatics[c.Scale.ChromaticNumber][t[i]%12]
		c.CName += c.Scale.Chromatics[c.Scale.ChromaticNumber][t[i]%12]
		//ret += c.Scale.NotePos(t[i])
		//ret += c.Scale.NotePos(tt[i])
		if i == 2 {
			break
		}
		ret += " "
	}
	//ret += ">"

	//c.CName = strings.Replace(c.CName, "#", "is", 5)
	//c.CName = strings.Replace(c.CName, "b", "es", 5)
	//c.CName = strings.ToLower(c.CName)

	//ret = strings.Replace(ret, "#", "is", 5)
	//ret = strings.Replace(ret, "b", "es", 5)
	//ret = strings.ToLower(ret)

	return ret
}
