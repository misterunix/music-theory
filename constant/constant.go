package constant

// Modes
const (
	// Ionian : 0, 2, 4, 5, 7, 9, 11
	Ionian = 0
	Major = 0
	// Dorian : 0, 2, 3, 5, 7, 9, 10
	Dorian = 1
	// Phrygian : 0, 1, 3, 5, 7, 8, 10
	Phrygian = 2
	// Lydian : 0, 2, 4, 6, 7, 9, 11
	Lydian = 3
	// Mixolydian : 0, 2, 4, 5, 7, 9, 10
	Mixolydian = 4
	// Aeolian : 0, 2, 3, 5, 7, 8, 10
	Aeolian = 5
	// Minor : 0, 2, 3, 5, 7, 8, 10
	Minor = 5
	// Locrian : 0, 1, 3, 5, 6, 8, 10
	Locrian = 6
)

// Interval Names / Steps
const (
	// IntervalP1 : Perfect Union
	IntervalP1 = 0
	// Intervalm2 : Minor Second
	Intervalm2 = 1
	// IntervalM2 : Major Seconf
	IntervalM2 = 2
	// Intervalm3 : Minor Third
	Intervalm3 = 3
	// IntervalM3 : Major Third
	IntervalM3 = 4
	// IntervalP4 : Perfect Fourth
	IntervalP4 = 5
	// Intervald5 : Dimished Fifth
	Intervald5 = 6
	// IntervalA4 : Augmented Fourth
	IntervalA4 = 6
	// IntervalP5 : Perfect Fifth
	IntervalP5 = 7
	// Intervalm6 : Minor Sixth
	Intervalm6 = 8
	// IntervalM6 : Major Sixth
	IntervalM6 = 9
	// Intervalm7 : Minor Seventh
	Intervalm7 = 10
	// IntervalM7 : Major Seventh
	IntervalM7 = 11
	// IntervalP8 : Perfect Union / Octive
	IntervalP8 = 12
	// Intervalm9 : Minor 9th
	Intervalm9 = 13
	// IntervalM9 : Minor 9th
	IntervalM9 = 14
	// Intervalm10 : Minor 10th
	Intervalm10 = 15
	// IntervalM10 : Major 10th
	IntervalM10 = 16
	// Intervalm11 : Minor 10th
	Intervalm11 = 17
	// IntervalM11 : Major 10th
	IntervalM11 = 18
	// Intervalm12 : Minor 10th
	Intervalm12 = 19
	// IntervalM12 : Major 10th
	IntervalM12 = 20
)

