package scales

import (
	"music-theory/constant"
)

// Data : Main struct for scale package
type Data struct {
	// Mode : constant.Ionian ... etc
	Mode int

	// Root : Current Root Note based on C = 0
	Root int

	BaseRoot        int
	Notes           [50]int
	RootIndex       int
	ScaleDegree     int
	ScaleName       string
	Chromatics      [4][12]string
	ChromaticNumber int
}

type ScaleTemplateData struct {
	KeyLarge  string
	LilyNotes string
	Key       string
}

// New : Create and return Data struct
func New() Data {
	d := Data{}
	return d
}

// Init : Place to initilize those hard to init vars
func (d *Data) Init(root int) {
	d.Chromatics = [4][12]string{
		{"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"},
		{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"},
		{"C", "C#", "D", "D#", "E", "E#", "F#", "G", "G#", "A", "A#", "B"},
		{"C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "Cb"},
	}
	d.Root = root
	d.BaseRoot = root % 12
	/*	for d.BaseRoot >= 1 {
			d.BaseRoot -= 12
		}
		d.BaseRoot += 12
	*/
	switch d.Mode {
	case constant.Ionian:
		{
			o := 0
			d.Notes[o] = d.BaseRoot
			o++
			for o <= 56-7 {
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 1
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 1
				o++
				//fmt.Println("o:", o)
				if o >= 49 {
					break
				}
			}
		}
	case constant.Aeolian:
		{
			o := 0
			d.Notes[o] = d.BaseRoot
			o++
			for o <= 56-7 {
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 1
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 1
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				d.Notes[o] = d.Notes[o-1] + 2
				o++
				//fmt.Println("o:", o)
				if o >= 49 {
					break
				}
			}
		}
	}
	var work [7]string
	for q := 0; q < 4; q++ {

		for i := 0; i < 7; i++ {
			work[i] = d.Chromatics[q][(d.Notes[i])%12]

		}
		bad := false
		for i := 0; i < 7; i++ {
			if work[i%7][0:1] == work[(i+1)%7][0:1] {
				bad = true

			}
		}
		if bad == true {
			continue
		} else {
			d.ChromaticNumber = q
			break
		}
	}

	for i, m := range d.Notes {
		if m == d.Root {
			d.RootIndex = i
			break
		}
	}
	d.ScaleName = d.Chromatics[d.ChromaticNumber][d.Notes[0]]
	switch d.Mode {
	case constant.Aeolian:
		d.ScaleName += "m"
	}
}

func (d *Data) GetNoteName(pos int) string {
	pos = pos % 12
	return d.Chromatics[d.ChromaticNumber][pos]
}

func (d *Data) NotePos(midinote int) string {
	//rr := midinote % 12
	rstring := ""
	notepos := int(float64(midinote/12.0)) - 4
	var count int
	if notepos != 0 {
		if notepos < 0 {
			count = notepos * -1
		} else {
			count = notepos
		}
		for j := 0; j < count; j++ {
			if notepos < 0 {
				rstring += ","
			}
			if notepos > 0 {
				rstring += "'"
			}
		}
	}
	return rstring
}
