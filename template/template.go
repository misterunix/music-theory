package template

import (
	"bytes"
	"log"

	//"music-theory/constant"
	"music-theory/scales"
	"os"
	"text/template"
)

type TemplateBase struct {
	fileHandler *os.File
	Blank       string
}

func Init() TemplateBase {
	b := TemplateBase{}
	os.Remove("book/book.lytex")
	f, err := os.OpenFile("book/book.lytex", os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Panicln(err)
	}
	b.fileHandler = f
	b.Blank = " "
	//defer f.Close()
	return b
}

func (b *TemplateBase) StartTemplates() {
	var tr bytes.Buffer
	var s string
	var err error

	g, err := template.ParseFiles("templates/header.template")
	if err != nil {
		log.Panicln(err)
	}

	err = g.Execute(&tr, b)
	if err != nil {
		panic(err)
	}
	s = tr.String()
	_, err = b.fileHandler.WriteString(s)
}

func (b *TemplateBase) EndTemplates() {
	var tr bytes.Buffer
	var s string
	var err error
	g, err := template.ParseFiles("templates/end.template")
	if err != nil {
		log.Panicln(err)
	}
	err = g.Execute(&tr, b)
	if err != nil {
		panic(err)
	}
	s = tr.String()
	_, err = b.fileHandler.WriteString(s)
	b.fileHandler.Close()
}

func (b *TemplateBase) PutScaleTemplate(std scales.ScaleTemplateData, mode int) {
	var tr bytes.Buffer
	var s string
	var err error

	fileName := "templates/scale.template"

	g, err := template.ParseFiles(fileName)
	if err != nil {
		log.Panicln(err)
	}
	err = g.Execute(&tr, std)
	if err != nil {
		log.Panicln(err)
	}
	s = tr.String()
	_, err = b.fileHandler.WriteString(s)
}
