package main

import (
	"fmt"
	"music-theory/chords"
	"music-theory/constant"
	"music-theory/template"
)


type Note struct {
	Name string
	MidiNumber int
	LilypondName string
}

func main() {

	t := template.Init()
	t.StartTemplates()


	for ModeIndex = constant.Ionian; ModeIndex<=constant.Locrian;ModeIndex++ {
		for Key = 0;k<12;k++ {
			scaledata := New()
			scaledata.Init(Key)


		}
	}
	for k := 0; k < 12; k++ {

		std := scales.ScaleTemplateData{}

		//t := 0
		c := chords.New()
		c.Init(k, constant.Ionian)

		fmt.Println("Key of", c.Scale.ScaleName)
		fmt.Printf("Scale,")
		for sn := 0; sn < 7; sn++ {
			tmp := c.Scale.Notes[sn] % 12
			if sn < 6 {
				fmt.Printf("%s,", c.Scale.Chromatics[c.Scale.ChromaticNumber][tmp])
			} else {
				fmt.Printf("%s", c.Scale.Chromatics[c.Scale.ChromaticNumber][tmp])
			}
		}
		fmt.Println()
		for i := 0; i < 7; i++ {
			c.GetChordName(i)
			switch c.Scale.Mode {
			case constant.Ionian:
				{
					fmt.Printf("%s,%s,%s,%s", c.ChordName, c.Triad(i, 0), c.Triad(i, 1), c.Triad(i, 2))
				}
			}
			fmt.Println()
		}
		fmt.Println()
	}

	for k := 0; k < 12; k++ {
		//t := 0
		c := chords.New()
		c.Init(k, constant.Aeolian)

		fmt.Println("Key of", c.Scale.ScaleName)
		fmt.Printf("Scale,")
		for sn := 0; sn < 7; sn++ {
			tmp := c.Scale.Notes[sn] % 12
			if sn < 6 {
				fmt.Printf("%s,", c.Scale.Chromatics[c.Scale.ChromaticNumber][tmp])
			} else {
				fmt.Printf("%s", c.Scale.Chromatics[c.Scale.ChromaticNumber][tmp])
			}
		}
		fmt.Println()

		for i := 0; i < 7; i++ {
			c.GetChordName(i)
			switch c.Scale.Mode {
			case constant.Aeolian:
				{
					fmt.Printf("%s,%s,%s,%s", c.ChordName, c.Triad(i, 0), c.Triad(i, 1), c.Triad(i, 2))
				}
			}
			fmt.Println()
		}
		fmt.Println()
	}

}
